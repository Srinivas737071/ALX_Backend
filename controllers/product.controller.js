const Products = require('../models/product.model');
const decodeBase64Image = require('../functions/helper');
//const bcrypt = require('bcrypt');
//Simple version, without validation or sanitation
exports.add = function (req, res) {
     var productData = Products({
            emailID: req.body.emailID,
            category: req.body.category,
            product: req.body.product,
            brand: req.body.brand,
            title: req.body.title,
            price: req.body.price,
            details: req.body.details,
            productLogo: new Buffer(decodeBase64Image.decodeBase64Image(req.body.productLogo).data, 'base64'),
            productLogoType: new Buffer(decodeBase64Image.decodeBase64Image(req.body.productLogo).type)           
        });
   productData.save((err, result) => {
            if (err) {
                return res.status(500).json({message: "Error saving to the db"});
            } else {
                res.status(200).json({result});
            }
        });
};


exports.availableProduct = (req, res) => {
    let offset = parseInt(req.query.offset) || 1;
    let id = req.query.id;
    let query = productData.aggregate([
        {"$match": {$and: [{"Status": {$ne: 'sale'}}]}},
        {
            $lookup:
                    {
                        from: 'users',
                        localField: 'emailID',
                        foreignField: 'emailID',
                        as: 'userDetails'
                    }
        },
        {"$sort": {createdOn: -1}}
    ]).exec(function (err, result) {
        if (err || result.length < 1 || result === []) {
            return res.status(404).json({message: "No Records Found"});
        } else {
            let data = result.map(function (item) {
                for (var i = 0; i < item.length; i++) {
                    item[i].productimage = "data:" + item[i].productLogoType + ";base64," + item[i].productLogo.toString("base64");
                    return item;
                }
            });
            var items = decodeBase64Image.Pagination(data, offset);
            res.status(200).json({result: items});
        }
    });
}