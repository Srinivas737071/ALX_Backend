const employeeModel = require('../models/user.model');
const jwt = require("jsonwebtoken");

//const bcrypt = require('bcrypt');
//Simple version, without validation or sanitation
exports.singUp = function (req, res) {
     var userData = employeeModel({
       emailID:req.body.emailID,
       userName:req.body.userName,
       loction:req.body.loction,
        });
   userData.save((err, result) => {
            if (err) {
                return res.status(500).json({message: "Error saving to the db"});
            } else {
                res.status(200).json({result});
            }
        });
};

exports.getUsersLocation = (req, res) => {
  employeeModel.find({ }, (err, result) => {
    if (err) {
      return res.json({ message: "Error in fetching data" });
    } else {
      res.json({ result });
    }
  });
};

exports.login = (req, res) => {
  if (!req.body.empEmail) {
    return res.status(400).json({ message: "please pass a proper Email id" });
  }
  employeeModel
    .findOne({ emailID: req.body.empEmail.toLowerCase()}, function(
      err,
      userData
    ) {
      if (err || userData === {} || userData === null) {
        res.status(404).json({ message: "User Not Found" });
      } else if (userData) {
        const payload = userData.employeeID
          ? { employeeID: userData.employeeID }
          : { emailID: userData.emailID };
        let token = jwt.sign(payload, process.env.SECRET);
        token = "JWT " + token;
        res
          .status(200)
          .json({
            status: 200,
            message: "Successfully logged in",
            token,
            userData
          });
      } else {
        res.status(404).json({ message: "User Not Found" });
      }
    })
    .select("-_id");
};