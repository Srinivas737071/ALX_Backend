// app.js
require("dotenv").config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const cors = require("cors");

// initialize our express app
const app = express();

let port = 3007;

mongoose.connect(
  process.env.db_url,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  function(err) {
    if (err) console.log("Error in connecting to DB");
    else console.log("Succesfully Connected");
  }
);
app.use(cors());
app.use(bodyParser.json({limit: '100mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});

app.get("/", (req, res) => {
  res.send("NodeJS");
});

const user = require("./routes/user.routers");
app.use("/user", user);

const product = require("./routes/product.routers");
app.use("/product", product);

