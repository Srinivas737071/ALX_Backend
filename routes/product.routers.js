const express = require("express");

let router = express.Router();
module.exports = router;


let products = require("../controllers/product.controller");

router.post("/add",(req,res)=>{
  products.add(req,res);
});

router.get("/availableProduct",(req,res)=>{
  products.availableProduct(req,res);
});