const express = require("express");

let router = express.Router();
module.exports = router;


let users = require("../controllers/user.controller");

router.post("/sigup",(req,res)=>{
  users.singUp(req,res);
});
router.get("/usersLocation",(req,res)=>{
  users.getUsersLocation(req,res);
});
router.post('/login', (req, res) => {
    users.login(req, res);
});