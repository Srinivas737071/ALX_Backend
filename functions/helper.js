function decodeBase64Image(dataString) {
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
  var response = {};

  if (matches.length !== 3) {
    return new Error("Invalid input string");
  }

  response.type = matches[1];
  response.data = new Buffer(matches[2], "base64");

  return response;
}

function Pagination(value, page) {
  var page = page || 1,
    per_page = 12,
    offset = (page - 1) * per_page,
    paginatedItems = value.slice(offset).slice(0, per_page),
    total_pages = Math.ceil(value.length / per_page);
  if (paginatedItems.length > 0)
    return {
      paginatedItems,
      pageNo: page,
      itemsPerPage: paginatedItems.length,
      totalCount: value.length,
      total_pages,
      per_page
    };
  else return null;
}

module.exports = { decodeBase64Image, Pagination };
