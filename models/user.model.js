const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UsersSchema = new Schema({
    emailID: { type: String, unique: true, required: true },
    userName: {type: String, required: true},
    loction: {type: String, required: true}
});
module.exports = mongoose.model('users', UsersSchema);