const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UsersSchema = new Schema({
    emailID: {type: String, required: true},
    category: {type: String, required: true},
    product: {type: String, required: true},
    brand: {type: String, required: true},
    title: {type: String, required: true},
    price: {type: String, required: true},
    details: {type: String, required: true},
    productLogo: {type: Buffer, default: null},
    productLogoType: String,
    Status:{type:String,default:"available"}
    },{
    timestamps: true,
    createdAt: true,
   updatedAt: { path: 'updatedAt', setOnInsert: false }
  });
module.exports = mongoose.model('products', UsersSchema);